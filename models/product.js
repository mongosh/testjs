const { Schema, model, SchemaType, SchemaTypes } = require("mongoose")

const ProductSchema = new Schema (
    {
        id: {
            type: SchemaTypes.ObjectId,
            required: true,
        },
        model: {
            type: String,
            required: true,
        },
        product_name: {
            type: String,
            required: true,
        },
        color: {
            type: String,
            required: true,
        },
        price: {
            type: String,
            required: true,
        },
    },
    { versionKey: false, timestamps: false }
);

const Products = model("products", ProductSchema)

module.exports = Products