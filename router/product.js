const Router = require("express")

const {
    AddProduct
} = require("../controller/products")

const router = Router()

router.post("/add", AddProduct)

module.exports = router