const checkId = require("../helper/check")
const SubCategory = require("../models/subcategory")
const Products = require("../models/product")

const AddProduct = async (req, res) => {
    try {
        const { id, model, product_name, color, price } = req.body
        if (!checkId(id))
            return res.send({
                status: 400,
                message: "Invalid id subcategoryid entered",
            })
        const subcat = await SubCategory.findOne({ _id: id })
        if (!subcat)
        return res.send({
            status: 400,
            message: "Non relevant subcategory id entered",
        })
        const Product = await Product(req.body)
        await Product.save()
        res.send({ status: 201, message: "Added", Product })
    }   catch (error) {
        console.log(error.message)
    }
}

module.exports = {
    AddProduct
}