const { isValidObjectId } = require("mongoose")

module.exports = (id) => {
    return isValidObjectId(id)
}